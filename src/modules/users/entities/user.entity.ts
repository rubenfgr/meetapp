import { IsBoolean, IsEmail, IsString } from "class-validator";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";


@Entity('users')
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: "varchar",
        length: 45,
        unique: true,
        nullable: false
    })
    @IsString()
    username: string;

    @Column({
        type: "varchar",
        length: 225,
        unique: true,
        nullable: false
    })
    @IsString()
    password: string;

    @Column({
        type: "varchar",
        length: 225,
        unique: true,
        nullable: false
    })
    @IsEmail()
    email: string;

    @Column({
        type: "boolean",
        default: true,
    })
    @IsBoolean()
    active: boolean;

}
