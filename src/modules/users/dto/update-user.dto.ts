import { IsString, IsEmail, IsBooleanString, IsBoolean } from "class-validator";


export class UpdateUserDto {

    @IsString()
    readonly username: string;

    @IsString()
    readonly password: string;

    @IsEmail()
    readonly email: string;

    @IsBoolean()
    readonly active: boolean;

}