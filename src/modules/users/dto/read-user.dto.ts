import { IsString, IsEmail, IsBoolean } from "class-validator";


export class ReadUserDto {

    @IsString()
    readonly username: string;

    @IsEmail()
    readonly email: string;

    @IsBoolean()
    readonly active: boolean;

}