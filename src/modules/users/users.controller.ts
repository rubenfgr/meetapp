import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { ReadUserDto, UpdateUserDto } from './dto';
import { CreateUserDto } from './dto/create-user.dto';
import { UsersService } from './users.service';



@Controller('users')
export class UsersController {

    constructor(private usersService: UsersService) { }

    @Get()
    getUsers(): Promise<ReadUserDto[]> {
        return this.usersService.getUsers();
    }

    @Get(':id')
    getUserById(@Param('id') id: number): Promise<ReadUserDto> {
        return this.usersService.getUserById(id);
    }

    @Get(':username')
    getUserByUsername(@Param('username') username: string): Promise<ReadUserDto> {
        return this.usersService.findByUsername(username);
    }

    @Get(':email')
    getUserByEmail(@Param('email') email: string): Promise<ReadUserDto> {
        return this.usersService.findByEmail(email);
    }

    @Post()
    create(@Body() dto: CreateUserDto): Promise<ReadUserDto> {
        return this.usersService.create(dto);
    }

    @Put(':id')
    update(@Param('id') id: number, @Body() updateUserDto: UpdateUserDto): Promise<ReadUserDto> {
        return this.usersService.update(id, updateUserDto);
    }

    @Delete(':id')
    delete(@Param('id') id): Promise<ReadUserDto> {
        return this.usersService.delete(id);
    }

    @Post('createusers')
    createUsers(): Promise<ReadUserDto[]> {
        return this.usersService.createUsers();
    }

}
