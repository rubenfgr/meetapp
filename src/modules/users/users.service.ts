import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { Repository } from 'typeorm';
import { ReadUserDto, UpdateUserDto } from './dto';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {

    constructor(
        @InjectRepository(User)
        private readonly _userRepository: Repository<User>) { }

    async getUsers(): Promise<ReadUserDto[]> {
        const users: User[] = await this._userRepository.find({ where: { active: true } });
        return users.map((user: User) => plainToClass(ReadUserDto, user));

    }

    async getUserById(id: number): Promise<ReadUserDto> {
        const user = await this._userRepository.findOne({ where: { id, active: true } });
        if (!user) {
            throw new NotFoundException(`User not found with id ${id}`);
        }
        return plainToClass(ReadUserDto, user);
    }

    async findByUsername(username: string): Promise<ReadUserDto> {
        const user = await this._userRepository.findOne({ where: { username, active: true } });
        if (!user) {
            throw new NotFoundException(`User not found with username ${username}`);
        }
        return plainToClass(ReadUserDto, user);
    }

    async findByEmail(email: string): Promise<ReadUserDto> {
        const user = await this._userRepository.findOne({ where: { email, active: true } });
        if (!user) {
            throw new NotFoundException(`User not found with email ${email}`);
        }
        return plainToClass(ReadUserDto, user);
    }

    async create(dto: CreateUserDto): Promise<ReadUserDto> {

        const user: User = await this._userRepository.createQueryBuilder('users')
            .where(dto.username)
            .orWhere(dto.email)
            .getOne();

        if (user) {
            throw new BadRequestException('Username and email must be unique.');
        }

        const newUser: User = plainToClass(User, dto);
        const savedUser = await this._userRepository.save(newUser);
        return plainToClass(ReadUserDto, savedUser);
    }

    async update(id: number, dto: UpdateUserDto): Promise<ReadUserDto> {
        const user: User = await this._userRepository.findOne(id, { where: { active: true } });
        if (!user) {
            throw new NotFoundException(`User not found with id ${id}`);
        }
        user.username = dto.username;
        user.password = dto.password;
        user.email = dto.email;
        user.active = dto.active;

        const updatedUser = await this._userRepository.save(user);
        return plainToClass(ReadUserDto, updatedUser);
    }

    async delete(id: number): Promise<ReadUserDto> {
        const user: User = await this._userRepository.findOne(id, { where: { active: true } });
        if (!user) {
            throw new NotFoundException(`User not found with id ${id}`);
        }
        user.active = false;

        const updatedUser = await this._userRepository.save(user);
        return plainToClass(ReadUserDto, updatedUser);
    }

    async createUsers(): Promise<ReadUserDto[]> {

        const users: User[] = [];

        for (let i = 0; i < 10; i++) {
            let user: User = new User();
            user.username = `test${i}`;
            user.password = `test${i}`;
            user.email = `test${i}@test.es`;
            try {
                await this._userRepository.save(user);
                users.push(user);
            } catch (Exception) {
                // ...
            }
        }

        return users.map((user: User) => plainToClass(ReadUserDto, user));
    }
}
