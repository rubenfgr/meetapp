import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './modules/users/dto/create-user.dto';
import { User } from './modules/users/entities/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class AppService {


  getIndex(): string {
    return `
      <h1>MeetApp</h1>
      <p>Create tests users:</p>
      <p><code>localhost:3000/users/createusers</code></p>
      <p><code>localhost:3000/users</code></p>
      `;
  }
}
