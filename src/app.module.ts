import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PagesModule } from './modules/pages/pages.module';
import { UsersModule } from './modules/users/users.module';

@Module({
  imports: [TypeOrmModule.forRoot(), UsersModule, PagesModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private connection: Connection) {
    if (connection.isConnected) console.log('OK. Connection to Mysql successfully');
    else console.log('ERROR. Connection to MySQL refused');
  }
}
